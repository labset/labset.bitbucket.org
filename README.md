# labset.bitbucket.org

[![Build Status](https://drone.io/bitbucket.org/labset/labset.bitbucket.org/status.png)](https://drone.io/bitbucket.org/labset/labset.bitbucket.org/latest)

**author**: Hasnae Rehioui (@hasnaer) 

random experiments and whatever crosses my mind or path

## structure

This blog is built using [HarpJS](http://harpjs.com/) since I am not really 
fond of the ruby ecosystem imposed by jekyll on github for instance ... "ruby gem pas vraiment"
booya frenchy nerd pun check !

The repo consists of three main branches :

- **harptemplates**: contains the templates, layouts, styles, and javascripts. 
All major edits to the latter resources are to be commited to this branch, 
later on merged to *harpcontent* branch.

- **harpcontent**: contains the actual contents served by HarpJS, that is,
non rendered html (markdown, jade, json). Commits to this branch trigger a *drone.io*
build responsible for backing up the existing site, compiling HarpJS site to static html,
deploying the latter to *master* branch.

- **master**: contains all the rendered markup to be served by bitbucket. There is no point
in manually commiting changes to this branch as they will eventually be overriden by any changes
commited in the upstream branches.

## workflow

- fork repo and push changes to either *harptemplates* or *harpcontent*
- send pull request
- @hasnaer or any labset volunteer to review and merge
- drone magic happens

This is not about breaking news, so yea I am happy with stuff taking its sweet ass time 
to build and be published.

## site builder

I have set up a hook between my pages repo on [bitbucket](http://bitbucket.org) and the kind continuous integration [drone.io](http://drone.io)

I configured the build to use **NodeJS 0.10** and filter on **harpcontent** branch

Bellow are the commands that drone build should execute

```bash
npm install -g -q harp     # installing harpjs in quiet mode
./_scripts/backup.sh       # backinup site in a new git branch
./_scripts/deploy.sh       # compiling harp and deploying to master
```

**backup.sh** and **deploay.sh** are commited to the repo on **harpcontent** branch
